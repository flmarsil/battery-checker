#BatteryChecker

Il est conseillé pour préserver la santé d'une batterie d'ordinateur portable de ne pas descendre en dessous de 40% et de ne pas monter au dessus de 80% de charge.

Cependant il est compliqué de garder un oeil sur notre batterie en permanence, et on se retrouve souvent au delas des limites conseillées ...

BatteryChecker vérifie l'état de charge de la batterie toutes les x secondes et envoi des notifications en boucle toute les x secondes lorsqu'il detecte un pourcentage de charge en dessous ou au dessus des limites.

Une fois le chargeur branché ou débranché, les notifications s'arrêtent.

Ce programme est codé pour 'MacOS' uniquement
