#include "../includes/BatteryChecker.hpp"

/* Singleton */
BatteryChecker* BatteryChecker::_Bm = nullptr;

BatteryChecker* BatteryChecker::getInstance() 
{
    (_Bm == nullptr) ? _Bm = new BatteryChecker() : 0;
    (_Bm->start() == FAILURE) ? sleep(SLEEP_ALERTE) : sleep(SLEEP_NO_ALERTE);
    return (_Bm);
}

/* Constructor */
BatteryChecker::BatteryChecker()
:   _batteryPercentage(0),
    _status(),
    _messageNotification(),
    _appleCommandLine(PMSET),
    _deleteFileCommandLine(RM),
    _scriptCommandLine(BASIC_SCRIPT),
    _alerte(false) {}

/* Destructor */
BatteryChecker::~BatteryChecker() {}

/* Methods */
int BatteryChecker::checkResult() 
{
    (getBatteryPercentage() <= MIN_PERCENTAGE_LIMIT && getStatus() == BATTERY_POWER)  ? _alerte = true : 0;
    (getBatteryPercentage() >= MAX_PERCENTAGE_LIMIT  && getStatus() == AC_POWER)      ? _alerte = true : 0;
    if (_alerte == true)
    {
        setMessageNotification();
        system(_messageNotification.c_str());   // send notification
        _messageNotification.clear();
        getStatus().clear();
        setBatteryPercentage(0);
        _alerte = false;
        return (FAILURE);
    }
    return (SUCCESS);
}

int BatteryChecker::start()
{
    std::ofstream file;
    file.open(".tmp", std::ios::out);
    if (!file.is_open())
        return (errorClose(ERROR_OPENING));
    system(getAppleCommandLine().c_str());
    file.close();
    if (parsing() == SUCCESS) 
        if (checkResult() == FAILURE)
            return (FAILURE);
    return (SUCCESS);
}

int BatteryChecker::parsing()
{
    std::ifstream file;
    std::vector<std::string> tokens;

    file.open(".tmp", std::ios::in);
    if (!file.is_open())
        return (system(getDeleteFileCommandLine().c_str()) && errorClose(ERROR_OPENING));
    while (file)
    {
        tokens = lineTokenizer(file);
        if (tokens[3] == AC_POWER || tokens[3] == BATTERY_POWER)
            setStatus(tokens[3]);
        else
            setBatteryPercentage(std::atoi(tokens[3].c_str()));
    }
    file.close();
    system(getDeleteFileCommandLine().c_str());
    return (SUCCESS);
}

std::vector<std::string> BatteryChecker::lineTokenizer(std::ifstream& file) 
{
    std::string bufferLine;
    std::getline(file, bufferLine);
    
    char* tk;
    std::vector<std::string> result;
    tk = std::strtok(&bufferLine[0], WHITESPACES);
    
    while (tk)
    {
        result.push_back(tk);
        tk = std::strtok(NULL, WHITESPACES);
    }
    return (result);
}

int BatteryChecker::errorClose (const std::string& message)
{
    std::cerr << RED << message << RESET << std::endl;
    return (EXIT_FAILURE);
}

// Setters 
void BatteryChecker::setMessageNotification() 
{
    std::string percentageStr;
    std::stringstream ss;

    ss << getBatteryPercentage();
    ss >> percentageStr;

    if (getBatteryPercentage() <= MIN_PERCENTAGE_LIMIT)
        _messageNotification = getScriptCommandLine() + "\"Batterie " + percentageStr + "%.\" with title " + NOTIF_TITLE + "subtitle " + NOTIF_SUB_TITLE_PLUG + "sound name " + NOTIF_SOUND + "'";
    else if (getBatteryPercentage() >= MAX_PERCENTAGE_LIMIT)
        _messageNotification = getScriptCommandLine() + "\"Batterie " + percentageStr + "%.\" with title " + NOTIF_TITLE + " subtitle " + NOTIF_SUB_TITLE_UNPLUG + " sound name " + NOTIF_SOUND + "'";
    return ;
}

void BatteryChecker::setBatteryPercentage(unsigned int percentage)      { _batteryPercentage = percentage; }
void BatteryChecker::setStatus(const std::string& status)               { _status = status; }

// Getters
unsigned int        BatteryChecker::getBatteryPercentage()      const   { return _batteryPercentage; }
std::string         BatteryChecker::getStatus()                 const   { return _status; }
const std::string&  BatteryChecker::getAppleCommandLine()       const   { return _appleCommandLine; }
const std::string&  BatteryChecker::getDeleteFileCommandLine()  const   { return _deleteFileCommandLine; }
const std::string&  BatteryChecker::getScriptCommandLine()      const   { return _scriptCommandLine; }
