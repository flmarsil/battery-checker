#include <iostream>
#include <unistd.h>
#include <fstream>
#include <sstream>
#include <vector>

#define WHITESPACES " \t\n\r\f\v\';-%"
#define RED "\033[1;31m"
#define RESET "\033[0m"

#define SUCCESS 0
#define FAILURE 1
#define MIN_PERCENTAGE_LIMIT 40
#define MAX_PERCENTAGE_LIMIT 80
#define SLEEP_ALERTE 7
#define SLEEP_NO_ALERTE 15

#define BATTERY_POWER "Battery"
#define AC_POWER "AC"
#define ERROR_OPENING "Error opening file"
#define PMSET "pmset -g batt > .tmp"
#define RM "rm .tmp"

#define BASIC_SCRIPT "osascript -e 'display notification "
#define NOTIF_TITLE "\"Battery Checker\""
#define NOTIF_SUB_TITLE_PLUG "\"Brancher le chargeur.\""
#define NOTIF_SUB_TITLE_UNPLUG "\"Débrancher le chargeur.\""
#define NOTIF_SOUND "\"Brise\""

class BatteryChecker
{
public:
    /* Destructor */
    ~BatteryChecker(); 
    /* Methods */
    int start();
    int parsing();
    int checkResult();
    int errorClose(const std::string& message);
    std::vector<std::string> lineTokenizer(std::ifstream& file);
    // Setters
    void setBatteryPercentage(unsigned int percentage);
    void setStatus(const std::string& status);
    void setMessageNotification();
    // Getters
    static BatteryChecker* getInstance();
    unsigned int getBatteryPercentage() const;
    std::string getStatus() const;
    const std::string& getAppleCommandLine() const;
    const std::string& getDeleteFileCommandLine() const;
    const std::string& getScriptCommandLine() const;

private:
    /* Constructor */
    BatteryChecker();
    /* Attributs */
    static BatteryChecker*  _Bm;
    unsigned int            _batteryPercentage;
    std::string             _status;
    std::string             _messageNotification;
    const std::string       _appleCommandLine;
    const std::string       _deleteFileCommandLine;
    const std::string       _scriptCommandLine;
    bool                    _alerte;
};  // class BatteryChecker
